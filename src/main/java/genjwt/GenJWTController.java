package genjwt;

import java.io.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.*;
import javax.naming.ldap.*;


@RestController
public class GenJWTController {

	Key key;

	public GenJWTController() {

		key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    Hashtable<String, String> environment = new Hashtable<String, String>();

    environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    environment.put(Context.PROVIDER_URL, "ldap://transpower.co.nz:389");
    environment.put(Context.SECURITY_AUTHENTICATION, "simple");
    environment.put(Context.SECURITY_PRINCIPAL, "TRANSPOWER\\thomsona");
    environment.put(Context.SECURITY_CREDENTIALS, "Hando2332(xx");
  
    try {
      DirContext ctx = new InitialDirContext(environment);
      System.out.println("Seems ok");
    } catch (Exception e) {
      System.out.println(e.getMessage());
    } finally {
    
    }

}

	@CrossOrigin(origins = "*", allowedHeaders = "*")
	@RequestMapping(value = "/issue", method = RequestMethod.POST)
	public ResponseEntity trigger(@RequestBody PostModel inputModel) {

		ResponseModel response = new ResponseModel();

    boolean works = validateCreds(inputModel.getUsername(), inputModel.getPassword());
    if (works) {
      System.out.println("Successfully logged in!");
    } else {
      System.out.println("Error logging in");
		  return new ResponseEntity(response, HttpStatus.FORBIDDEN);
    }

    // TODO: Get group memberships
		// TODO: Map group memberships to scopes

		Instant now = Instant.now();
		Instant _exp = now.plus(2, ChronoUnit.HOURS);
		Date nbf = Date.from(now);
		Date exp = Date.from(_exp);

		String jws = Jwts.builder()
			.setSubject(inputModel.getUsername())
			.claim("scp", "scope1 scope2 scope3")
			.setIssuer("issueralex")
			.setNotBefore(nbf)
			.setExpiration(exp)
			.signWith(key)
			.compact();

		response.jwt = jws;

		return new ResponseEntity(response, HttpStatus.OK);

	}

  private boolean validateCreds(String username, String password) {

    Hashtable<String, String> environment = new Hashtable<String, String>();

    environment.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
    environment.put(Context.PROVIDER_URL, "ldap://transpower.co.nz:389");
    environment.put(Context.SECURITY_AUTHENTICATION, "simple");
    environment.put(Context.SECURITY_PRINCIPAL, "TRANSPOWER\\"+username);
    environment.put(Context.SECURITY_CREDENTIALS, password);

    System.out.println("UN: "+username);
    System.out.println("PW: "+password);

    try {
      DirContext ctx = new InitialDirContext(environment);

      String search = "dc=transpower,dc=co,dc=nz";
      String filter = "sAMAccountName="+username;

      String[] returningAttrs = { "memberof" };
      SearchControls ctls = new SearchControls();
      ctls.setReturningObjFlag(true);
      ctls.setReturningAttributes(returningAttrs);
      ctls.setSearchScope(SearchControls.OBJECT_SCOPE);

      ctx.search(search, filter, ctls);

    } catch (Exception e) {
      System.out.println(e.getMessage());
      return false;
    }

    return true;
   
  }

}
